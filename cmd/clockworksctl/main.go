/*
Package main is the main executable
*/
package main

import (
	"os"

	"gitlab.oit.duke.edu/devil-ops/clockworksctl/cmd/clockworksctl/cmd"
)

func main() {
	if err := cmd.NewRootCmd().Execute(); err != nil {
		os.Exit(1)
	}
}
