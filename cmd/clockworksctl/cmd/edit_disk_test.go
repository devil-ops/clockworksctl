package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func TestValidateDisks(t *testing.T) {
	// Single item
	got, err := validateDisks(&clockworks.VM{
		Disks: clockworks.NewDiskSet([]int{100}),
	}, 0)
	require.NoError(t, err)
	require.Equal(t, &clockworks.VMDisk{
		DiskID: 0,
		SizeGB: 100,
	}, got)

	// Match item with multiple disks
	got, err = validateDisks(&clockworks.VM{
		Disks: clockworks.NewDiskSet([]int{100, 50}),
	}, 1)
	require.NoError(t, err)
	require.Equal(t, &clockworks.VMDisk{
		DiskID: 1,
		SizeGB: 50,
	}, got)
}
