package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMobToConsoleURL(t *testing.T) {
	ts := []struct {
		mob     string
		want    string
		wantErr bool
	}{
		{
			mob:  "https://vmware-test.example.com/mob/?moid=vm-3904",
			want: "vmrc://vmware-test.example.com:443/?moid=vm-3904",
		},
		{
			mob:     "foo",
			want:    "",
			wantErr: true,
		},
	}

	for _, tt := range ts {
		got, err := urlWithMOB(tt.mob)
		if tt.wantErr {
			require.Error(t, err)
		} else {
			require.NoError(t, err)
		}
		if got != tt.want {
			require.Equal(t, tt.want, got)
		}
	}
}
