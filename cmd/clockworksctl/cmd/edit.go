package cmd

import (
	"github.com/spf13/cobra"
)

// NewEditCmd represents the edit command
func NewEditCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "edit",
		Short: "Edit something in Clockworks",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, _ []string) {
			fatalUsage(cmd)
		},
	}
	bindSkipWait(cmd)

	bindVMEditCmd(cmd)

	editDiskCmd := NewEditDiskCmd()
	cmd.AddCommand(editDiskCmd)
	return cmd
}
