package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// NewDeleteVMCmd represents the deleteVM command
func NewDeleteVMCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "vm",
		Short:   "Delete a VM from Clockworks",
		Example: `$ clockworksctl delete vm foo-01.example.com`,
		Args:    cobra.ExactArgs(1),
		ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			if client != nil {
				return client.VM.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
			}
			return nil, cobra.ShellCompDirectiveNoFileComp
		},
		Run: func(cmd *cobra.Command, args []string) {
			ensureClient()
			skipConfirm, err := cmd.Flags().GetBool("skip-confirm")
			checkErr(err, "")
			wait, err := cmd.Flags().GetBool("wait")
			checkErr(err, "")

			opts, err := client.VMDeleteOptsWithCmd(cmd, args)
			checkErr(err, "Could not creat options with the given command")

			vm, _, err := client.VM.GetWithFQDN(ctx, args[0])
			checkErr(err, "Could not find a VM with that name")

			// Make the edit if it's cool
			if !skipConfirm {
				fmt.Println("⚠️ WARNING️  About to delete VM:", vm.Name)
				fmt.Println("  ", vm.WebURL)
				if !askForConfirmationX("Are you sure you want to delete this VM?") {
					return
				}
			}

			reqR, _, err := client.VM.Delete(ctx, vm.ID, opts)
			checkErr(err, "")

			if wait {
				err = client.VMRequest.WaitForFinalStatus(ctx, reqR.VMRequestID)
				checkErr(err, "")
			} else {
				logger.Info("request submitted", "url", reqR.VMRequestWebURL)
			}
		},
	}
	err := clockworks.BindDeleteVMCobra(cmd)
	if err != nil {
		panic(err)
	}
	return cmd
}
