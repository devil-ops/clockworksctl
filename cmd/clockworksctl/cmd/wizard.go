package cmd

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/huh/spinner"
	"github.com/drewstinnett/gout/v2"
	"github.com/sourcegraph/conc"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

var wizardCmd = &cobra.Command{
	Use:   "wizard",
	Short: "Use a wizard for clockworks actions",
	Long: `Interesting things to note when using the Wizard:

If you have a long list of options, you can filter by pressing '/' and typing a string to match

Consider this beta for now, unsure if it all totally works, but open to feedback!`,
	Aliases: []string{"w"},
	Args:    cobra.NoArgs,
	RunE: func(_ *cobra.Command, _ []string) error {
		ensureClient()
		logger.Warn("!!wizard mode is still in BETA!!")
		action, err := wizardSelect()
		if err != nil {
			return err
		}
		switch action {
		case "create":
			return createVMWizard()
		case "delete":
			return deleteVMWizard()
		default:
			return fmt.Errorf("don't know how to handle this action yet: %v", action)
		}
	},
}

func wizardSelect() (string, error) {
	var ret string
	form := huh.NewSelect[string]().
		Title("Would king of action would you like to perform?").
		Options(
			huh.Option[string]{
				Key:   "🐣 Create a new VM",
				Value: "create",
			},
			huh.Option[string]{
				Key:   "💀 Delete an existing VM",
				Value: "delete",
			},
		).
		Value(&ret)
	if err := form.Run(); err != nil {
		return "", err
	}
	return ret, nil
}

func deleteVMWizard() error {
	opts := newDeleteFormOpts(client)
	if err := opts.populateHostnames(); err != nil {
		return err
	}

	if err := opts.deleteForm().Run(); err != nil {
		return err
	}

	gout.MustPrint(opts.hostS.Hosts[opts.HostnameID])

	var confirm bool
	checkErr(huh.NewForm(
		huh.NewGroup(
			huh.NewConfirm().
				Title("💀 Delete the VM above? 💀").
				Affirmative("Yes!").
				Negative("No.").
				Value(&confirm),
		),
	).Run(), "error running confirmation form")

	if !confirm {
		logger.Warn("quitting...")
		return nil
	}
	logger.Info("Deleting vm!")

	after, err := strconv.Atoi(opts.archiveDays)
	if err != nil {
		return err
	}
	reqInfo, _, err := client.VM.Delete(ctx, opts.HostnameID, clockworks.NewVMDeleteOpts(
		clockworks.WithDeleteArchiveAfter(time.Hour*time.Duration(after)),
	))
	if err != nil {
		return err
	}
	rctx, cancel := context.WithCancel(ctx)
	var wg conc.WaitGroup
	wg.Go(func() {
		checkErr(client.VMRequest.WaitForFinalStatus(rctx, reqInfo.VMRequestID), "error waiting for vm to be deleted")
		cancel()
	})

	checkErr(spinner.New().
		Type(spinner.Line).
		Title("Deleting your VM! 💀").
		Context(rctx).
		Run(), "error running spinner")

	wg.Wait()
	return nil
}

func createVMWizard() error {
	opts := newCreateFormOpts(client)

	if err := opts.Populate(); err != nil {
		return err
	}

	logger.Info("Checking clockworks for information to build the wizard")

	if err := opts.CreateForm().Run(); err != nil {
		return err
	}

	vmReq, err := opts.VMCreate()
	if err != nil {
		return err
	}

	gout.MustPrint(vmReq)

	var confirm bool
	checkErr(huh.NewForm(
		huh.NewGroup(
			huh.NewConfirm().
				Title("Create a VM with the above specs?").
				Affirmative("Yes!").
				Negative("No.").
				Value(&confirm),
		),
	).Run(), "error running confirmation form")

	if !confirm {
		logger.Warn("quitting...")
		return nil
	}

	logger.Info("Creating vm!")

	reqInfo, _, err := client.VM.Create(ctx, vmReq)
	if err != nil {
		return err
	}
	rctx, cancel := context.WithCancel(ctx)
	var wg conc.WaitGroup
	wg.Go(func() {
		checkErr(client.VMRequest.WaitForFinalStatus(rctx, reqInfo.VMRequestID), "error waiting for vm to be created")
		cancel()
	})

	checkErr(spinner.New().
		Type(spinner.Line).
		Title("Making your brand new VM! 💻").
		Context(rctx).
		Run(), "error running spinner")

	wg.Wait()
	return nil
}
