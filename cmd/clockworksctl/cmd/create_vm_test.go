package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCreateVM(t *testing.T) {
	t.Setenv("CLOCKWORKS_USERNAME", "foo")
	t.Setenv("CLOCKWORKS_TOKEN", "bar")
	got, err := lazyClient("env")
	require.NoError(t, err)
	require.NotNil(t, got)

	_ = NewRootCmd()
	/*
		r.SetArgs([]string{
			"create", "vm",
		})
		err = r.Execute()
		require.NoError(t, err)
	*/
}
