package cmd //nolint:golint,dupl

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getContainerCmd represents the getContainer command
var getContainerCmd = &cobra.Command{
	Use:     "container [[NAME]..]",
	Short:   "Retrieve container information",
	Aliases: []string{"c", "containers"},
	Args:    cobra.NoArgs,
	Run: func(_ *cobra.Command, _ []string) {
		ensureClient()
		items, _, err := client.Container.List(ctx)
		checkErr(err, "")

		gout.MustPrint(items)
	},
}

func init() {
	getCmd.AddCommand(getContainerCmd)
}
