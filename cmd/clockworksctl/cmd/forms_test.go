package cmd

import (
	"testing"

	"github.com/charmbracelet/huh"
	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func TestValidateHostname(t *testing.T) {
	require.NoError(t, validateHostname("foo.example.com"))
	require.EqualError(t, validateHostname(""), "must enter a hostname")
	require.EqualError(t, validateHostname("foo"), "must be an fqdn")
}

func TestLocationSelector(t *testing.T) {
	ls := newLocationSelector()
	ls.Add(clockworks.Location{
		DisplayName: "some location",
		ID:          5,
	})
	ls.Add(clockworks.Location{
		DisplayName: "some other location",
		ID:          6,
	})
	require.Equal(t, []string{"some location", "some other location"}, ls.Hints)
	require.NoError(t, ls.Validate("some location"))
	require.EqualError(t, ls.Validate(""), "location must be set")
	require.EqualError(t, ls.Validate("never exists"), "not a valid location: never exists")
}

func TestFormToRequest(t *testing.T) {
	ls := newLocationSelector()
	ls.Add(
		clockworks.Location{
			ID:          5,
			DisplayName: "Some location",
		},
		clockworks.Location{
			ID:          6,
			DisplayName: "Some Other location",
		},
	)
	got, err := formOpts{
		Hostname:       "foo.bar",
		FundCode:       "000-1111",
		RAM:            "3",
		CPU:            "2",
		OSID:           "foo",
		StorageSize:    "100",
		CloudInit:      "#cloud-init",
		StorageType:    2,
		SysadminOption: 5,
		Location:       5,
		locationS:      ls,
	}.VMCreate()
	require.NoError(t, err)
	require.EqualValues(
		t, "000-1111", got.FundCode,
	)
}

func TestCreateForm(t *testing.T) {
	ls := newLocationSelector()
	ls.Locations = map[int]clockworks.Location{
		5: {
			DisplayName: "foo",
			ID:          5,
		},
	}
	f := formOpts{
		client: clockworks.New(
			clockworks.WithToken("foo"),
			clockworks.WithUser("bar"),
		),
		locationS: ls,
		osOpts: map[string][]huh.Option[string]{
			"linux":   {{}},
			"windows": {{}},
		},
		containerOpts:   []huh.Option[string]{{}},
		networkOpts:     []huh.Option[string]{{}},
		patchWindowOpts: []huh.Option[int]{{}},
	}
	require.NotPanics(
		t,
		func() {
			f.CreateForm()
		},
	)
	require.NoError(t, f.Populate())
}
