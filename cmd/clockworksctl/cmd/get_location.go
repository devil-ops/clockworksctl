package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// getLocationCmd represents the getLocation command
var getLocationCmd = &cobra.Command{
	Use:     "location [[NAME]..]",
	Short:   "Retrieve location information",
	Aliases: []string{"l", "locations"},
	ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if client != nil {
			return client.Location.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
		}
		return nil, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(_ *cobra.Command, args []string) {
		ensureClient()
		var items []clockworks.Location

		if len(args) > 0 {
			for idx, arg := range args {
				item, _, err := client.Location.GetWithName(ctx, arg)
				checkErr(err, "")
				items[idx] = *item
			}
		} else {
			var err error
			items, _, err = client.Location.List(ctx)
			checkErr(err, "")
		}
		gout.MustPrint(items)
	},
}

func init() {
	getCmd.AddCommand(getLocationCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getLocationCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getLocationCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
