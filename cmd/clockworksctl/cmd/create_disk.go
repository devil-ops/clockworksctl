package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// NewCreateDiskCmd represents the createDisk command
func NewCreateDiskCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "disk VM_NAME --size SIZE_GB",
		Short: "Create a new disk on an existing VM",
		Args:  cobra.ExactArgs(1),
		ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			if client != nil {
				return client.VM.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
			}
			return nil, cobra.ShellCompDirectiveNoFileComp
		},
		Run: func(cmd *cobra.Command, args []string) {
			ensureClient()
			skipConfirm, err := cmd.Flags().GetBool("skip-confirm")
			checkErr(err, "")
			newSize, err := cmd.Flags().GetInt("size")
			checkErr(err, "")
			wait, err := cmd.Flags().GetBool("wait")
			checkErr(err, "")
			vm, _, err := client.VM.GetWithFQDN(ctx, args[0])
			checkErr(err, "")
			opt := &clockworks.VMEditOpts{
				Disks: clockworks.NewDiskSetPTR([]int{newSize}),
			}
			if !skipConfirm {
				fmt.Println("About to edit VM:", vm.Name)
				fmt.Println("  ", vm.WebURL)
				if !askForConfirmationX("Are you sure you want to edit this VM?") {
					return
				}
			}

			editReqR, _, err := client.VM.Edit(ctx, vm.ID, opt)
			checkErr(err, "")

			if wait {
				err = client.VMRequest.WaitForFinalStatus(ctx, editReqR.VMRequestID)
				checkErr(err, "")
			} else {
				logger.Info("request submitted", "url", editReqR.VMRequestWebURL)
			}
		},
	}

	cmd.PersistentFlags().Int("size", 0, "Size of disk in Gb")
	err := cmd.MarkPersistentFlagRequired("size")
	if err != nil {
		panic(err)
	}

	return cmd
}
