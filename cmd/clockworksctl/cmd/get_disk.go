package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getDiskCmd represents the getDisk command
var getDiskCmd = &cobra.Command{
	Use:     "disk VM_NAME",
	Short:   "Get disk information on a server",
	Args:    cobra.ExactArgs(1),
	Aliases: []string{"d", "disks"},
	ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if client != nil {
			return client.VM.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
		}
		return nil, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(_ *cobra.Command, args []string) {
		ensureClient()
		vm, _, err := client.VM.GetWithFQDN(ctx, args[0])
		checkErr(err, "")
		gout.MustPrint(vm.Disks)
	},
}

func init() {
	getCmd.AddCommand(getDiskCmd)
}
