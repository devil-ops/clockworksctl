package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// getOSCmd represents the getOS command
var getOSCmd = &cobra.Command{
	Use:     "os [[NAME]..]",
	Short:   "Retrieve OS information",
	Aliases: []string{"o", "oses"},
	ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if client != nil {
			return client.OS.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
		}
		return nil, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(_ *cobra.Command, args []string) {
		ensureClient()
		var items []clockworks.OS
		var err error

		if len(args) > 0 {
			for _, arg := range args {
				var item *clockworks.OS
				item, _, err = client.OS.Get(ctx, arg)
				checkErr(err, "")
				items = append(items, *item)
			}
		} else {
			items, _, err = client.OS.List(ctx)
		}
		checkErr(err, "")
		gout.MustPrint(items)
	},
}

func init() {
	getCmd.AddCommand(getOSCmd)
}

/*
func getOses(cmd *cobra.Command, tc string) []string {
	initClient()
	items, _, err := client.OS.List(context.TODO())
	if err != nil {
		log.Warn().Err(err).Send()
	}
	var ret []string
	for _, i := range items {
		if strings.Contains(i.OSId, tc) {
			ret = append(ret, fmt.Sprintf("%v\t%v", i.OSId, i.FullName))
		}
	}
	return ret
}
*/
