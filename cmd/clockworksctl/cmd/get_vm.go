package cmd

import (
	"context"
	"strconv"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// NewGetVMCmd represents the getVM command
func NewGetVMCmd() *cobra.Command {
	return &cobra.Command{
		Use:     "vm [[ID_OR_FQDN]..]",
		Short:   "Retrieve vm information",
		Aliases: []string{"v", "vms"},
		ValidArgsFunction: func(_ *cobra.Command, args []string, _ string) ([]string, cobra.ShellCompDirective) {
			ensureClient()
			if len(args) != 0 {
				return nil, cobra.ShellCompDirectiveNoFileComp
			}
			return client.VM.ShellComplete(context.TODO(), ""), cobra.ShellCompDirectiveNoFileComp
		},
		RunE: func(_ *cobra.Command, args []string) error {
			ensureClient()
			if len(args) > 0 {
				items := make([]clockworks.VM, len(args))
				for idx, arg := range args {
					var id int
					var item *clockworks.VM
					id, err := strconv.Atoi(arg)
					if err == nil {
						item, _, err = client.VM.Get(ctx, id)
					} else {
						item, _, err = client.VM.GetWithFQDN(ctx, arg)
					}
					if err != nil {
						return err
					}
					items[idx] = *item
				}
				gout.MustPrint(items)
			} else {
				items, _, err := client.VM.List(ctx, clockworks.NewVMListOpts())
				if err != nil {
					return err
				}
				gout.MustPrint(items)
			}
			return nil
		},
	}
}
