package cmd

import (
	"errors"
	"fmt"
	"os"
	"slices"
	"sort"
	"strconv"
	"strings"

	"github.com/charmbracelet/huh"
	"github.com/sourcegraph/conc"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// formOpts is similar to clockworks.VMCreateOpts, but handles only types that work with the form. It should be converted to a VMCreateOpts before sending to clockworks
type formOpts struct {
	RAM             string
	CPU             string
	Hostname        string
	HostnameID      int
	OnBehalfOf      string
	Application     string
	FundCode        string
	Container       string
	ProductionLevel int
	Location        int
	SysadminOption  int
	OSFamily        string
	OSID            string
	StorageType     int
	StorageSize     string
	CloudInit       string
	NetworkType     int
	PatchWindow     int
	Subnet          string
	useCloudInit    bool
	archiveDays     string
	// Little helpers to fill out the form
	locationS       *locationSelector
	hostS           *hostSelector
	networkOpts     []huh.Option[string]
	osOpts          map[string][]huh.Option[string]
	containerOpts   []huh.Option[string]
	patchWindowOpts []huh.Option[int]
	hostnameOpts    []huh.Option[int]
	client          *clockworks.Client
}

func sysadminOpts() []huh.Option[int] {
	return []huh.Option[int]{
		// Need to select a container as well if this is selected
		huh.NewOption("Self Administration", 1),
		huh.NewOption("8/5 Standard OIT-SI OS System Administration", 2),
		huh.NewOption("24/7 Standard OIT-SI OS System Administration", 3),
		huh.NewOption("8/5 Web Hosting OIT-SI OS System Administration", 4),
		huh.NewOption("24/7 Web Hosting OIT-SI OS System Administration", 5),
	}
}

func functionOpts() []huh.Option[int] {
	return []huh.Option[int]{
		huh.NewOption("Development", 3),
		huh.NewOption("Test", 2),
		huh.NewOption("Production", 1),
	}
}

func osOpts() []huh.Option[string] {
	return []huh.Option[string]{
		huh.NewOption("Linux", "linux"),
		huh.NewOption("Windows", "windows"),
	}
}

func storageOpts() []huh.Option[int] {
	return []huh.Option[int]{
		huh.NewOption("Bronze", 3),
		huh.NewOption("Silver", 2),
		huh.NewOption("Platinum", 1),
	}
}

func networkOpts() []huh.Option[int] {
	return []huh.Option[int]{
		huh.NewOption("Private", 2),
		huh.NewOption("Public", 1),
		huh.NewOption("Custom", 3),
	}
}

func newDeleteFormOpts(c *clockworks.Client) *formOpts {
	return &formOpts{
		archiveDays: "30",
		Hostname:    os.Getenv("CLOCKWORKS_HOSTNAME"),
		client:      c,
		hostS:       newHostSelector(),
	}
}

func newCreateFormOpts(c *clockworks.Client) *formOpts {
	return &formOpts{
		Hostname:    os.Getenv("CLOCKWORKS_HOSTNAME"),
		FundCode:    os.Getenv("CLOCKWORKS_FUNDCODE"),
		RAM:         "2",
		CPU:         "2",
		StorageSize: "50",
		locationS:   newLocationSelector(),
		CloudInit:   "#cloud-config",
		osOpts: map[string][]huh.Option[string]{
			"windows": {},
			"linux":   {},
		},
		client: c,
	}
}

func (f *formOpts) behalfOfInput() *huh.Input {
	return huh.NewInput().
		Title("NetID of the on behalf of user").
		Value(&f.OnBehalfOf)
}

func (f *formOpts) cloudInitConfirm() *huh.Confirm {
	return huh.NewConfirm().
		Title("Use Cloud Init?").
		Value(&f.useCloudInit)
}

func (f *formOpts) networkInput() *huh.Select[int] {
	return huh.NewSelect[int]().
		Title("Network Type").
		Options(networkOpts()...).
		Value(&f.NetworkType)
}

func (f *formOpts) patchWindowSelect() *huh.Select[int] {
	return huh.NewSelect[int]().
		Title("Patch Window").
		Options(f.patchWindowOpts...).
		Value(&f.PatchWindow)
}

func (f *formOpts) hostnameInput() *huh.Input {
	return huh.NewInput().
		Title("Hostname for the VM").
		Validate(validateHostname).
		Value(&f.Hostname)
}

func (f *formOpts) applicationInput() *huh.Input {
	return huh.NewInput().
		Title("Application").
		Value(&f.Application)
}

func (f *formOpts) fundCodeInput() *huh.Input {
	return huh.NewInput().
		Title("Fund Code").
		Validate(validateFundCode).
		Value(&f.FundCode)
}

func (f *formOpts) functionInput() *huh.Select[int] {
	return huh.NewSelect[int]().
		Title("Server Function").
		Options(functionOpts()...).
		Value(&f.ProductionLevel)
}

func (f *formOpts) locationInput() *huh.Select[int] {
	return huh.NewSelect[int]().
		Title("Location").
		Height(min(len(f.locationS.Locations)+2, 10)).
		Options(f.locationS.Options...).
		Value(&f.Location)
}

func (f *formOpts) operatingSystemInput() *huh.Select[string] {
	return huh.NewSelect[string]().
		Title("Operating System").
		Options(osOpts()...).
		Value(&f.OSFamily)
}

func (f *formOpts) sysadminInput() *huh.Select[int] {
	return huh.NewSelect[int]().
		Title("Sysadmin Option").
		Options(sysadminOpts()...).
		Value(&f.SysadminOption)
}

func (f *formOpts) containerInput() *huh.Select[string] {
	return huh.NewSelect[string]().
		Title("Folder for Self-Admin Host").
		Options(f.containerOpts...).
		Value(&f.Container)
}

func (f *formOpts) linuxFamilyInput() *huh.Select[string] {
	return huh.NewSelect[string]().
		Title("Linux Version").
		Options(f.osOpts["linux"]...).
		Value(&f.OSID)
}

func (f *formOpts) windowsFamilyInput() *huh.Select[string] {
	return huh.NewSelect[string]().
		Title("Windows Version").
		Options(f.osOpts["windows"]...).
		Value(&f.OSID)
}

func (f *formOpts) ramInput() *huh.Input {
	return huh.NewInput().
		Title("RAM (in GB)").
		Validate(validateNumber).
		Value(&f.RAM)
}

func (f *formOpts) cpuInput() *huh.Input {
	return huh.NewInput().
		Title("CPU Count").
		Validate(validateNumber).
		Value(&f.CPU)
}

func (f *formOpts) storageSizeInput() *huh.Input {
	return huh.NewInput().
		Title("Storage Size (in Gb)").
		Validate(validateNumber).
		Value(&f.StorageSize)
}

func (f *formOpts) storageTypeInput() *huh.Select[int] {
	return huh.NewSelect[int]().
		Title("Storage Type").
		Options(storageOpts()...).
		Value(&f.StorageType)
}

func (f *formOpts) cloudInitInput() *huh.Text {
	return huh.NewText().
		Title("Cloud Init User Data").
		Value(&f.CloudInit)
}

func (f *formOpts) customNetworkSelect() *huh.Select[string] {
	return huh.NewSelect[string]().
		Title("Custom Network").
		Options(f.networkOpts...).
		Value(&f.Subnet)
}

func (f *formOpts) newCreateGeneralGroup() *huh.Group {
	return huh.NewGroup(
		f.behalfOfInput(), f.hostnameInput(), f.applicationInput(),
		f.fundCodeInput(), f.functionInput(), f.locationInput(),
		f.networkInput(), f.operatingSystemInput(), f.sysadminInput(),
		f.cloudInitConfirm(),
	)
}

func (f *formOpts) newCreateContainerGroup() *huh.Group {
	return huh.NewGroup(f.containerInput()).WithHideFunc(func() bool {
		return f.SysadminOption != int(clockworks.SelfAdminSupport)
	})
}

func (f *formOpts) newCreateLinuxGroup() *huh.Group {
	return huh.NewGroup(f.linuxFamilyInput()).WithHideFunc(func() bool {
		return f.OSFamily != "linux"
	})
}

func (f *formOpts) newCreateWindowsGroup() *huh.Group {
	return huh.NewGroup(f.windowsFamilyInput()).WithHideFunc(func() bool {
		return f.OSFamily != "windows"
	})
}

func (f *formOpts) newCreateSizingGroup() *huh.Group {
	return huh.NewGroup(
		f.ramInput(), f.cpuInput(),
		f.storageSizeInput(), f.storageTypeInput(),
	)
}

func (f *formOpts) newCreateCloudInitGroup() *huh.Group {
	return huh.NewGroup(
		f.cloudInitInput(),
	).WithHideFunc(func() bool {
		return !f.useCloudInit
	})
}

func (f *formOpts) newCreateCustomNetworkGroup() *huh.Group {
	return huh.NewGroup(f.customNetworkSelect()).WithHideFunc(func() bool {
		return f.NetworkType != int(clockworks.CustomNetwork)
	})
}

func (f *formOpts) newCreatePatchwindowGroup() *huh.Group {
	return huh.NewGroup(f.patchWindowSelect()).WithHideFunc(func() bool {
		return f.SysadminOption == int(clockworks.SelfAdminSupport)
	})
}

func (f *formOpts) deleteForm() *huh.Form {
	if len(f.hostnameOpts) == 0 {
		panic("no hosts found")
	}
	return huh.NewForm(
		huh.NewGroup(
			huh.NewSelect[int]().
				Title("Host to delete").
				Options(f.hostnameOpts...).
				Value(&f.HostnameID),
			huh.NewInput().
				Title("Delete Archive After N Days").
				Validate(validateNumber).
				Value(&f.archiveDays),
		),
	)
}

// CreateForm returns a huh.CreateForm using the createFormsOpts
func (f *formOpts) CreateForm() *huh.Form {
	return huh.NewForm(
		f.newCreateGeneralGroup(),
		f.newCreateContainerGroup(),
		f.newCreateLinuxGroup(),
		f.newCreateWindowsGroup(),
		f.newCreateSizingGroup(),
		f.newCreateCloudInitGroup(),
		f.newCreatePatchwindowGroup(),
		f.newCreateCustomNetworkGroup(),
	)
}

/*
// VMDelete returns a VMDeleteOpts from a formOpts
func (f formOpts) VMDelete(vm clockworks.VM) (*clockworks.VMDeleteOpts, error) {

}
*/

// VMCreate returns a VMCreateOpts from a formOpts
func (f formOpts) VMCreate() (*clockworks.VMCreateOpts, error) {
	ram, err := strconv.Atoi(f.RAM)
	if err != nil {
		return nil, err
	}
	cpu, err := strconv.Atoi(f.CPU)
	if err != nil {
		return nil, err
	}
	storageGB, err := strconv.Atoi(f.StorageSize)
	if err != nil {
		return nil, err
	}
	opts := []clockworks.Option[clockworks.VMCreateOpts]{
		clockworks.WithRAM[clockworks.VMCreateOpts](ram),
		clockworks.WithCPU[clockworks.VMCreateOpts](cpu),
		clockworks.WithHostname[clockworks.VMCreateOpts](f.Hostname),
		clockworks.WithOnBehalfOf[clockworks.VMCreateOpts](f.OnBehalfOf),
		clockworks.WithApplication[clockworks.VMCreateOpts](f.Application),
		clockworks.WithFundCode[clockworks.VMCreateOpts](f.FundCode),
		clockworks.WithProductionLevel[clockworks.VMCreateOpts](clockworks.ProductionLevel(f.ProductionLevel)),
		clockworks.WithLocation[clockworks.VMCreateOpts](f.locationS.Locations[f.Location]),
		clockworks.WithSysadminOption[clockworks.VMCreateOpts](clockworks.SysadminOption(f.SysadminOption)),
		clockworks.WithOSID[clockworks.VMCreateOpts](f.OSID),
		clockworks.WithStorageType[clockworks.VMCreateOpts](clockworks.StorageType(f.StorageType)),
		clockworks.WithDisks[clockworks.VMCreateOpts](clockworks.NewDiskSet([]int{storageGB})),
		clockworks.WithCloudInitUserData[clockworks.VMCreateOpts](f.CloudInit),
		clockworks.WithNetworkType[clockworks.VMCreateOpts](clockworks.NetworkType(f.NetworkType)),
	}
	if f.NetworkType == int(clockworks.CustomNetwork) {
		opts = append(opts, clockworks.WithSubnet[clockworks.VMCreateOpts](f.Subnet))
	}
	if f.SysadminOption == int(clockworks.SelfAdminSupport) {
		opts = append(opts, clockworks.WithContainer[clockworks.VMCreateOpts](f.Container))
	}

	o := clockworks.NewVMCreateOpts[clockworks.VMCreateOpts](opts...)
	return o, nil
}

func validateHostname(s string) error {
	if s == "" {
		return errors.New("must enter a hostname")
	}
	if !strings.Contains(s, ".") {
		return errors.New("must be an fqdn")
	}
	return nil
}

type locationSelector struct {
	Hints     []string
	Options   []huh.Option[int]
	Locations map[int]clockworks.Location
}

type hostSelector struct {
	Options []huh.Option[int]
	Hosts   map[int]clockworks.VM
}

func newHostSelector() *hostSelector {
	return &hostSelector{
		Hosts: map[int]clockworks.VM{},
	}
}

func newLocationSelector() *locationSelector {
	return &locationSelector{
		Locations: map[int]clockworks.Location{},
	}
}

func (s *locationSelector) Add(ls ...clockworks.Location) {
	for _, l := range ls {
		s.Options = append(s.Options, huh.Option[int]{
			Key:   l.DisplayName,
			Value: l.ID,
		})
		s.Hints = append(s.Hints, strings.ToLower(l.DisplayName))
		s.Locations[l.ID] = l
	}
}

func (s locationSelector) Validate(l string) error {
	l = strings.ToLower(l)
	if l == "" {
		return errors.New("location must be set")
	}
	if !slices.Contains(s.Hints, l) {
		return fmt.Errorf("not a valid location: %v", l)
	}
	return nil
}

func validateNumber(s string) error {
	_, err := strconv.Atoi(s)
	return err
}

func validateFundCode(s string) error {
	if s == "" {
		return errors.New("must set fund code")
	}
	return nil
}

func (f *formOpts) populatePatchWindows() error {
	pws, _, err := f.client.PatchWindow.List(ctx)
	if err != nil {
		return err
	}
	f.patchWindowOpts = make([]huh.Option[int], len(pws))
	for idx, pw := range pws {
		f.patchWindowOpts[idx] = huh.Option[int]{
			Key:   pw.Name,
			Value: int(pw.ID),
		}
	}
	return nil
}

func (f *formOpts) populateHostnames() error {
	hosts, _, err := f.client.VM.List(ctx, clockworks.NewVMListOpts[clockworks.VMListOpts]())
	if err != nil {
		return err
	}
	if len(hosts) == 0 {
		return errors.New("no VMs found, are you sure you have access?")
	}
	f.hostnameOpts = make([]huh.Option[int], len(hosts))
	sort.Slice(hosts, func(i, j int) bool {
		return hosts[i].Name < hosts[j].Name
	})
	for idx, h := range hosts {
		f.hostnameOpts[idx] = huh.Option[int]{
			Key:   h.Name,
			Value: h.ID,
		}
		f.hostS.Hosts[h.ID] = h
	}
	return nil
}

func (f *formOpts) populateLocations() error {
	locations, _, err := f.client.Location.List(ctx)
	if err != nil {
		return err
	}
	checkErr(err, "could not determine locations")
	f.locationS.Add(locations...)
	return nil
}

func (f *formOpts) populateContainers() error {
	containers, _, err := f.client.Container.List(ctx)
	if err != nil {
		return err
	}
	f.containerOpts = make([]huh.Option[string], len(containers))
	for idx, container := range containers {
		f.containerOpts[idx] = huh.Option[string]{
			Key:   container.Name,
			Value: container.Name,
		}
	}
	return nil
}

func (f *formOpts) populateNetworks() error {
	networks, _, err := f.client.Network.List(ctx)
	if err != nil {
		return err
	}
	f.networkOpts = make([]huh.Option[string], len(networks))
	for idx, network := range networks {
		f.networkOpts[idx] = huh.Option[string]{
			Key:   fmt.Sprintf("%v (%v)", network.Name, network.Subnet),
			Value: network.Subnet,
		}
	}
	return nil
}

func (f *formOpts) populateOses() error {
	oses, _, err := f.client.OS.List(ctx)
	if err != nil {
		return err
	}

	// Select the OS based off of a previously filtered OS value
	for _, osI := range oses {
		family := strings.TrimSuffix(osI.Family, "Guest")
		if _, ok := f.osOpts[family]; ok {
			f.osOpts[family] = append(f.osOpts[family], huh.Option[string]{
				Key:   fmt.Sprintf("%v : %v", osI.FullName, osI.Movement),
				Value: osI.OSId,
			})
		} else {
			f.client.Logger.Warn("ignoring unknown os family", "family", family)
		}
	}
	return nil
}

func (f *formOpts) Populate() error {
	var wg conc.WaitGroup
	if len(f.patchWindowOpts) == 0 {
		wg.Go(func() {
			checkErr(f.populatePatchWindows(), "could not populate patch windows")
		})
	}
	if len(f.locationS.Locations) == 0 {
		wg.Go(func() {
			checkErr(f.populateLocations(), "could not populate locations")
		})
	}
	if len(f.containerOpts) == 0 {
		wg.Go(func() {
			checkErr(f.populateContainers(), "could not populate containers")
		})
	}

	if len(f.networkOpts) == 0 {
		wg.Go(func() {
			checkErr(f.populateNetworks(), "could not populate networks")
		})
	}

	if (len(f.osOpts["linux"]) == 0) && (len(f.osOpts["windows"]) == 0) {
		wg.Go(func() {
			checkErr(f.populateOses(), "could not populate operating systems")
		})
	}
	wg.Wait()
	return nil
}
