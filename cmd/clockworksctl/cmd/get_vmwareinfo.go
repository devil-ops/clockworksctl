package cmd

import (
	"context"
	"net/url"
	"strconv"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// NewGetVMWareInfoCmd represents the 'get vmware-info' command
func NewGetVMWareInfoCmd() *cobra.Command {
	return &cobra.Command{
		Use:     "vmware-info [[ID_OR_FQDN]..]",
		Short:   "Retrieve VMWare information for a given vm",
		Args:    cobra.ExactArgs(1),
		Aliases: []string{"vmwareinfo", "vmi"},
		ValidArgsFunction: func(_ *cobra.Command, args []string, _ string) ([]string, cobra.ShellCompDirective) {
			ensureClient()
			if len(args) != 0 {
				return nil, cobra.ShellCompDirectiveNoFileComp
			}
			return client.VM.ShellComplete(context.TODO(), ""), cobra.ShellCompDirectiveNoFileComp
		},
		RunE: func(_ *cobra.Command, args []string) error {
			ensureClient()
			var item *clockworks.VM
			id, err := strconv.Atoi(args[0])
			if err == nil {
				item, _, err = client.VM.Get(ctx, id)
			} else {
				item, _, err = client.VM.GetWithFQDN(ctx, args[0])
			}
			if err != nil {
				return err
			}
			u, err := url.Parse(item.MobURL)
			if err != nil {
				return err
			}

			gout.MustPrint(vmwareInfo{
				VSPhereURL: u.Host,
				MoRef:      u.Query().Get("moid"),
			})
			return nil
		},
	}
}

type vmwareInfo struct {
	VSPhereURL string `yaml:"vsphere-url" json:"vsphere-url"`
	MoRef      string `yaml:"mo-ref" json:"mo-ref"`
}
