package cmd

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// getPatchWindowCmd represents the getPatchWindow command
var getPatchWindowCmd = &cobra.Command{
	Use:     "patchwindow [[ID]..]",
	Short:   "Retrieve patch window information",
	Aliases: []string{"pw", "patchwindows"},
	Args:    cobra.NoArgs,
	Run: func(_ *cobra.Command, _ []string) {
		ensureClient()
		var items []clockworks.PatchWindow
		var err error

		items, _, err = client.PatchWindow.List(ctx)
		checkErr(err, "")
		gout.MustPrint(items)
	},
}

func init() {
	getCmd.AddCommand(getPatchWindowCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getPatchWindowCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getPatchWindowCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
