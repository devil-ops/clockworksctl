package cmd //nolint:golint,dupl

import (
	"context"
	"fmt"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// getNetworkCmd represents the getNetwork command
var getNetworkCmd = &cobra.Command{
	Use:     "network [[CIDR]..]",
	Short:   "Retrieve network information",
	Aliases: []string{"n", "networks"},
	ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if client != nil {
			return client.Network.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
		}
		return nil, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(_ *cobra.Command, args []string) {
		ensureClient()
		var networks []clockworks.Network

		if len(args) > 0 {
			for idx, arg := range args {
				network, _, err := client.Network.Get(ctx, arg)
				checkErr(err, fmt.Sprintf("coult not get specific network: %v", arg))
				networks[idx] = *network
			}
		} else {
			var err error
			networks, _, err = client.Network.List(ctx)
			checkErr(err, "")
		}
		gout.MustPrint(networks)
	},
}

func init() {
	getCmd.AddCommand(getNetworkCmd)
}
