package cmd

import (
	"context"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// NewEditDiskCmd represents the editDisk command
func NewEditDiskCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "disk [VM_NAME] --size NEW_SIZE",
		Short: "Edit a disk attached to a VM",
		Long:  ``,
		Args:  cobra.ExactArgs(1),
		ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			if client != nil {
				return client.VM.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
			}
			return nil, cobra.ShellCompDirectiveNoFileComp
		},
		Run: func(cmd *cobra.Command, args []string) {
			ensureClient()

			skipConfirm, wait := getSkipWait(cmd)

			vm, _, err := client.VM.GetWithFQDN(ctx, args[0])
			checkErr(err, "")
			targetDisk, err := validateDisks(vm, mustGetCmd[int](*cmd, "disk"))
			checkErr(err, "")

			if !skipConfirm {
				if !askForConfirmationX(fmt.Sprintf(`About to edit VM: %v
   %v
Are you sure you want to edit this VM?
`, vm.Name, vm.WebURL)) {
					return
				}
			}
			editReqR, _, err := client.VM.Edit(ctx, vm.ID, clockworks.NewVMEditOpts(clockworks.WithDisks[clockworks.VMEditOpts](clockworks.DiskSet{
				clockworks.VMDisk{
					DiskID: targetDisk.DiskID,
					SizeGB: mustGetCmd[int](*cmd, "size"),
				},
			})))
			checkErr(err, "")

			logger.Info("request submitted", "url", editReqR.VMRequestWebURL)
			if wait {
				checkErr(client.VMRequest.WaitForFinalStatus(ctx, editReqR.VMRequestID), "error waiting for final status")
			}
		},
	}
	cmd.PersistentFlags().Int("disk", 0, "The disk id to edit")
	cmd.PersistentFlags().Int("size", 0, "New size of disk in Gb")
	err := cmd.MarkPersistentFlagRequired("size")
	panicIfErr(err)
	return cmd
}

func validateDisks(vm *clockworks.VM, diskID int) (*clockworks.VMDisk, error) {
	numDisks := len(vm.Disks)
	switch {
	case numDisks == 1:
		return &vm.Disks[0], nil
	case diskID > 0:
		// For hosts with multiple disks, we need to find the disk we want
		for _, d := range vm.Disks {
			d := d
			if d.DiskID == diskID {
				return &d, nil
			}
		}
		return nil, errors.New("could not find a disk with that id")
	case (diskID == 0) && (numDisks > 1):
		return nil, errors.New("must specify a disk id to edit")
	default:
		panic("unexpected thing here, investigate if this panic is really needed")
	}
}
