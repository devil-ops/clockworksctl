package cmd

import (
	"fmt"
	"os"
	"testing"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
)

func TestNewRootCmdBadArg(t *testing.T) {
	r := NewRootCmd()
	require.NotNil(t, r)

	r.SetArgs([]string{"never-exists"})

	err := r.Execute()
	require.Error(t, err)
	require.EqualError(t, err, `unknown command "never-exists" for "clockworksctl"`)
}

func TestLazyClient(t *testing.T) {
	os.Clearenv()
	got, _ := lazyClient("env")
	require.Nil(t, got)

	t.Setenv("CLOCKWORKS_USERNAME", "foo")
	t.Setenv("CLOCKWORKS_TOKEN", "bar")
	var err error
	got, err = lazyClient("env")
	fmt.Fprintf(os.Stderr, "%+v", clientErr)
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestSkipWait(t *testing.T) {
	cmd := &cobra.Command{}
	bindSkipWait(cmd)
	cmd.SetArgs([]string{"--skip-confirm", "--wait"})
	err := cmd.Execute()
	require.NoError(t, err)
	gotS, gotW := getSkipWait(cmd)
	require.True(t, gotS)
	require.True(t, gotW)
}

// TestMain initializes the test bits
func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func setup() {
}

func shutdown() {
}
