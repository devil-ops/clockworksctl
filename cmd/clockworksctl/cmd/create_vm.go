package cmd

import (
	"errors"
	"fmt"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// NewCreateVMCmd represents the createVM command
func NewCreateVMCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "vm HOSTNAME",
		Short: "Create a VM in Clockworks",
		Long: `Disks:

For disks, by default, provisions a single 50 Gb disk. If you specify the --disk
parameter, the first disk will use this new value.

Single disk of 50 Gb default:
$ clockworksctl create vm ...

Single disk of 100 Gb:
$ clockworksctl create vm --disk 100 ...

25 Gb initial disk and a 50 Gb additional disk:
$ clockworksctl create vm --disk 25 --disk 50 ...`,
		Args: cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			ensureClient()
			var err error
			skipConfirm := mustGetCmd[bool](*cmd, "skip-confirm")
			wait := mustGetCmd[bool](*cmd, "wait")

			opt, err := client.VMCreateOptsWithCmd(cmd, args)
			checkErr(err, "")

			if !skipConfirm && !askForConfirmation("About to create this VM, are you sure you want to continue?", prettyVMRequest(*opt)) {
				return errors.New("quitting due to no confirmation")
			}

			reqR, _, err := client.VM.Create(ctx, opt)
			if err != nil {
				return err
			}

			if wait {
				if err := prettyWait(
					fmt.Sprintf("Building %v...\n\nSee here for more info: %v", opt.Hostname, reqR.VMRequestWebURL),
					func() {
						if werr := client.VMRequest.WaitForFinalStatus(ctx, reqR.VMRequestID); werr != nil {
							panic(werr)
						}
					},
				); err != nil {
					return err
				}

				logger.Info("completed create request")
			} else {
				logger.Info("request submitted", "url", reqR.VMRequestWebURL)
			}
			gout.MustPrint(reqR)
			return nil
		},
	}
	err := clockworks.BindNewVMCobra(cmd, client)
	if err != nil {
		panic(err)
	}
	return cmd
}
