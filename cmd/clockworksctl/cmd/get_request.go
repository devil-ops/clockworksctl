package cmd

import (
	"strconv"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

// NewGetRequestCmd returns the command for getting requests
func NewGetRequestCmd() *cobra.Command {
	return &cobra.Command{
		Use:     "request ID [ID...]",
		Short:   "Retrieve vm information",
		Aliases: []string{"r", "requests", "req", "reqs"},
		Args:    cobra.MinimumNArgs(1),
		RunE: func(_ *cobra.Command, args []string) error {
			ensureClient()
			items := make([]clockworks.VMRequest, len(args))
			for idx, arg := range args {
				id, err := strconv.Atoi(arg)
				if err != nil {
					return err
				}
				item, _, err := client.VMRequest.Get(ctx, id)
				if err != nil {
					return err
				}
				items[idx] = *item
			}
			gout.MustPrint(items)
			return nil
		},
	}
}
