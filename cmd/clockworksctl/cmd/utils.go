package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"net/url"
	"os"
	"reflect"
	"strings"
	"time"

	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/huh/spinner"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/lipgloss/table"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func askForConfirmation(title, desc string) bool {
	var ret bool
	form := huh.NewForm(
		huh.NewGroup(
			huh.NewConfirm().
				Title(title).
				Description(desc).
				Value(&ret),
		),
	)
	if err := form.Run(); err != nil {
		panic(err)
	}
	return ret
}

func prettyWait(title string, action func()) error {
	if err := spinner.New().
		Title(title).
		TitleStyle(lipgloss.NewStyle()).
		Action(action).
		Run(); err != nil {
		return err
	}
	return nil
}

func prettyVMRequest(opts clockworks.VMCreateOpts) string {
	re := lipgloss.NewRenderer(os.Stdout)
	evenStyle := re.NewStyle()
	oddStyle := re.NewStyle().Background(lipgloss.Color("252"))
	rows := [][]string{
		{"Hostname", string(opts.Hostname)},
		{"CPU", fmt.Sprint(opts.CPU)},
		{"RAM", fmt.Sprint(opts.RAM)},
		{"Backups", fmt.Sprint(opts.Backups)},
		{"Disaster Recovery", fmt.Sprint(opts.Backups)},
		{"Operating System", fmt.Sprint(opts.OSID)},
		{"Fund Code", string(opts.FundCode)},
	}
	t := table.New().
		Border(lipgloss.NormalBorder()).
		Width(80).
		StyleFunc(func(row, _ int) lipgloss.Style {
			switch {
			case row%2 == 0:
				return evenStyle
			default:
				return oddStyle
			}
		}).
		// BorderStyle(lipgloss.NewStyle().Foreground(lipgloss.Color("99"))).
		// Headers("LANGUAGE", "FORMAL").
		Rows(rows...)
	return t.String()
}

// askForConfirmationX is the old way of asking confirmation.
// Deprecated: use askForConfirmation instead
func askForConfirmationX(s string) bool {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Printf("%s [y/n]: ", s)

		response, err := reader.ReadString('\n')
		if err != nil {
			panic("error reading newline from string")
		}

		response = strings.ToLower(strings.TrimSpace(response))

		if response == "y" || response == "yes" {
			return true
		} else if response == "n" || response == "no" {
			return false
		}
	}
}

// urlWithMOB takes a MOB and converts it to the console URL
func urlWithMOB(mob string) (string, error) {
	u, err := url.Parse(mob)
	if err != nil {
		return "", err
	}
	if u.Host == "" {
		return "", errors.New("could not pull host from url")
	}
	mobid := u.Query().Get("moid")
	return fmt.Sprintf("vmrc://%v:443/?moid=%v", u.Host, mobid), nil
}

func fatalUsage(cmd *cobra.Command) {
	fmt.Fprint(os.Stderr, cmd.UsageString())
	os.Exit(1)
}

/*
// autocom is just a helper for the autocomplete functions
type autocom func(*cobra.Command, []string, string) ([]string, cobra.ShellCompDirective)

// autocomData holds more auto complete info
type autocomData struct {
	Operator autocom
	Command  *cobra.Command
}

// enhanceAutocomplete adds real autocomplete in, for things that need the client to be active
func enhanceAutocomplete(a map[string]*autocomData, c *clockworks.Client) {
	if c != nil {
		// Patch Windows
		a["patch-window-id"].Operator = func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			return c.PatchWindow.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveDefault
		}

			a["location"].Operator = func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
				return c.Location.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveDefault
			}

			a["os"].Operator = func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
				return c.OS.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveDefault
			}
	}
	for k, ops := range a {
		fmt.Fprintf(os.Stderr, "OP: %+v\n", ops.Command.Flags())
		err := createCmd.RegisterFlagCompletionFunc(k, ops.Operator)
		CheckErr(err, "error marking flag completion for os")
	}
}
*/

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

// mustGetCmd uses generics to get a given flag with the appropriate Type from a cobra.Command
func mustGetCmd[T []int | []string | int | string | bool | time.Duration](cmd cobra.Command, s string) T {
	switch any(new(T)).(type) {
	case *int:
		item, err := cmd.Flags().GetInt(s)
		panicIfErr(err)
		return any(item).(T)
	case *string:
		item, err := cmd.Flags().GetString(s)
		panicIfErr(err)
		return any(item).(T)
	case *bool:
		item, err := cmd.Flags().GetBool(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]int:
		item, err := cmd.Flags().GetIntSlice(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]string:
		item, err := cmd.Flags().GetStringSlice(s)
		panicIfErr(err)
		return any(item).(T)
	case *time.Duration:
		item, err := cmd.Flags().GetDuration(s)
		panicIfErr(err)
		return any(item).(T)
	default:
		panic(fmt.Sprintf("unexpected use of mustGetCmd: %v", reflect.TypeOf(s)))
	}
}
