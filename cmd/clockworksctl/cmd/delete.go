package cmd

import (
	"github.com/spf13/cobra"
)

// NewDeleteCmd represents the delete command
func NewDeleteCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "delete",
		Short: "Delete items from Clockworks",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, _ []string) {
			fatalUsage(cmd)
		},
	}
	bindSkipWait(cmd)
	cmd.AddCommand(NewDeleteVMCmd())
	return cmd
}
