package cmd

import (
	"github.com/spf13/cobra"
)

// NewCreateCmd represents the create command
func NewCreateCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "create",
		Short:   "Create an object in Clockworks",
		Aliases: []string{"c", "add", "a"},
		Args:    cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, _ []string) {
			fatalUsage(cmd)
		},
	}
	bindSkipWait(cmd)

	cmd.AddCommand(NewCreateDiskCmd())
	cmd.AddCommand(NewCreateVMCmd())

	return cmd
}
