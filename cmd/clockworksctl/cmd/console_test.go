package cmd

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func testCopyFile(f string, w io.Writer) {
	rp, err := os.ReadFile(f)
	panicIfErr(err)
	_, err = io.Copy(w, bytes.NewReader(rp))
	panicIfErr(err)
}

func TestConsoleCmd(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case strings.HasSuffix(r.URL.Path, "/api/v1/vms/1"):
			w.Write([]byte(`{"id": 1, "name": "acme-bigip-02.example.com", "mob_url": "https://vmware-test.example.com/mob/?moid=vm-3904"}`))
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/vms"):
			testCopyFile("./testdata/vms.json", w)
			return
		default:
			panic(fmt.Sprintf("unknown how to handle %v", r.URL.Path))
		}
		/*
			w.WriteHeader(200)
			content, err := os.ReadFile("testdata/vms.json")
			require.NoError(t, err)
			w.Write(content)
		*/
	}))
	defer tsrv.Close()

	t.Setenv("CLOCKWORKS_USERNAME", "foo")
	t.Setenv("CLOCKWORKS_TOKEN", "bar")
	t.Setenv("CLOCKWORKS_BASE_URL", tsrv.URL)
	client, _ = lazyClient("env")
	require.NotNil(t, client)
	cmd := NewConsoleCmd()
	require.NotNil(t, cmd)
	cmd.SetArgs([]string{"foo.example.com", "--dry-run"})

	err := cmd.Execute()
	require.NoError(t, err)
}
