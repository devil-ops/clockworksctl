package cmd

import (
	"context"
	"errors"
	"fmt"
	"os/exec"
	"runtime"

	"github.com/spf13/cobra"
)

// NewConsoleCmd represents the console command
func NewConsoleCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "console VM_NAME",
		Short: "Open a VMWare console to the specified VM",
		Args:  cobra.ExactArgs(1),
		ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			if client != nil {
				return client.VM.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
			}
			return nil, cobra.ShellCompDirectiveNoFileComp
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			ensureClient()

			dryRun, err := cmd.Flags().GetBool("dry-run")
			if err != nil {
				return err
			}
			vm, _, err := client.VM.GetWithFQDN(ctx, args[0])
			if err != nil {
				return err
			}

			vmURL, err := urlWithMOB(vm.MobURL)
			if err != nil {
				return err
			}
			fmt.Println(vmURL)

			if !dryRun {
				switch runtime.GOOS {
				case "linux":
					err = exec.Command("xdg-open", vmURL).Start() //nolint:gosec
				case "windows":
					err = exec.Command("rundll32", "url.dll,FileProtocolHandler", vmURL).Start() //nolint:gosec
				case "darwin":
					err = exec.Command("open", vmURL).Start() //nolint:gosec
				default:
					logger.Error("not supported on this os", "os", runtime.GOOS)
					return errors.New("unsupported os")
				}
				if err != nil {
					return err
				}
			}
			return nil
		},
	}

	cmd.PersistentFlags().Bool("dry-run", false, "Don't actually launch the console, just print the URL")

	return cmd
}
