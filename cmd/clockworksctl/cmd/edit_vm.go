package cmd

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
)

// NewEditVMCmd represents the editVM command
func NewEditVMCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "vm VM_NAME|ID",
		Short: "Edit a VM in Clockworks",
		Args:  cobra.ExactArgs(1),
		ValidArgsFunction: func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			if client != nil {
				return client.VM.ShellComplete(context.TODO(), toComplete), cobra.ShellCompDirectiveNoFileComp
			}
			return nil, cobra.ShellCompDirectiveNoFileComp
		},
		Run: func(cmd *cobra.Command, args []string) {
			ensureClient()

			skipConfirm, wait := getSkipWait(cmd)

			// editReq := &clockworks.VMEditOpts{}
			editReq, err := client.VMEditOptsWithCmd(cmd, args)
			checkErr(err, "could not create edit request from cobra.Command")

			// Create a human readable-ish string for the request
			d, err := json.Marshal(editReq)
			checkErr(err, "")

			vm, _, err := client.VM.GetWithFQDN(ctx, args[0])
			checkErr(err, "")

			// Make the edit if it's cool
			if !skipConfirm {
				fmt.Println("About to edit VM:", vm.Name)
				fmt.Println("  ", vm.WebURL)
				fmt.Printf("Edit request: %+v\n", string(d))
				if !askForConfirmationX("Are you sure you want to edit this VM?") {
					return
				}
			}
			logger.Info("editing vm", "vm", vm.Name)

			editReqR, _, err := client.VM.Edit(ctx, vm.ID, editReq)
			checkErr(err, "")

			if wait {
				err = client.VMRequest.WaitForFinalStatus(ctx, editReqR.VMRequestID)
				checkErr(err, "")
			} else {
				logger.Info("request submitted", "url", editReqR.VMRequestWebURL)
			}
		},
	}
}
