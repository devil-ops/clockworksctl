/*
Package cmd is the main holder for all the clockworksctl commands
*/
package cmd

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"os"

	"github.com/charmbracelet/log"

	"github.com/spf13/cobra"

	goutbra "github.com/drewstinnett/gout-cobra"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

var (
	version   = "dev"
	cfgFile   string
	client    *clockworks.Client
	ctx       context.Context
	clientErr any
	rootCmd   *cobra.Command
	logger    *slog.Logger
)

// Verbose increases the logging output
var (
	Verbose bool
)

// NewRootCmd returns a new Root cobra.Command
func NewRootCmd() *cobra.Command {
	rcmd := &cobra.Command{
		Use:          "clockworksctl",
		Short:        "Interact with the Clockworks API",
		Version:      version,
		SilenceUsage: true,
		PersistentPreRun: func(cmd *cobra.Command, _ []string) {
			checkErr(goutbra.Cmd(cmd, goutbra.WithField("format")), "error binding to cobra")

			outputCurl, err := cmd.Flags().GetBool("output-curl-string")
			checkErr(err, "")
			if outputCurl {
				err := os.Setenv("CLOCKWORKS_CURL_PRINT", "1")
				checkErr(err, "Could not set CLOCKWORKS_CURL_PRINT env var")
			}
		},
		PersistentPostRun: func(_ *cobra.Command, _ []string) {
			spam := os.Getenv("CLOCKWORKS_SPAM")
			if spam != "" {
				fmt.Fprint(os.Stderr, "\nLike what you see here? You too can use clockworksctl! Check it out from here:\n 💥 https://gitlab.oit.duke.edu/devil-ops/clockworksctl 💥\n")
			}
		},
	}

	// Generic stuff bind here
	bindGlobalFlags(rcmd)

	// Bind cobra arguments to Gout
	mustBindCobraGout(rcmd)

	// Console commands
	rcmd.AddCommand(NewConsoleCmd())

	// Delete commands
	rcmd.AddCommand(NewDeleteCmd())

	// Edit Stuff
	rcmd.AddCommand(NewEditCmd())

	// Get
	rcmd.AddCommand(getCmd)
	getCmd.AddCommand(
		NewGetVMCmd(),
		NewGetRequestCmd(),
		NewGetVMWareInfoCmd(),
	)

	// Wizard
	rcmd.AddCommand(wizardCmd)

	// Create Pieces
	rcmd.AddCommand(NewCreateCmd())

	// Set global
	rootCmd = rcmd
	return rcmd
}

func init() {
	ctx = context.Background()
	initLogging()

	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cli" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".clockworksctl")
	}

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix("clockworks")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		logger.Debug("using config file", "file", viper.ConfigFileUsed())
	}

	// Lazily get a new client for use in autocomplete operations
	initClient()

	initCompletion()

	// Completion binding must be done AFTER the client is initialized
}

func findSubCmd(cmd *cobra.Command, verb, noun string) (*cobra.Command, error) {
	for _, verbC := range cmd.Commands() {
		if verbC.Use == verb {
			for _, nounC := range verbC.Commands() {
				if nounC.Use == noun {
					return nounC, nil
				}
			}
		}
	}
	return nil, errors.New("could not find sub command")
}

func initCompletion() {
	if rootCmd != nil {
		scmd, err := findSubCmd(rootCmd, "create", "vm HOSTNAME")
		if err != nil {
			logger.Warn("could not find create vm subcommand", "err", err)
		}
		err = clockworks.BindCreateVMCompletions(scmd, client)
		if err != nil {
			logger.Warn("could not bind completions", "err", err)
		}
	}
}

// checkErr logs a fatal message if an error is present
func checkErr(err error, msg string) {
	if err != nil {
		logger.Error(msg, "err", err)
		os.Exit(2)
	}
}

// lazyClient returns a client if it has enough viper data to do so, otherwise,
// returns an error.  This is useful for the autocomplete stuff that is a
// nice-to-have, but not necessary to run
// func lazyClient() (c *clockworks.Client, err error) {
func lazyClient(am string) (c *clockworks.Client, err error) {
	defer func() {
		if err := recover(); err != nil {
			// Fail silently
			// log.Warn().Interface("err", err).Send()
			clientErr = err
			return
		}
	}()
	switch am {
	case "viper":
		c = clockworks.New(clockworks.WithViper(viper.GetViper()))
	case "env":
		c = clockworks.New(clockworks.WithEnv())
	default:
		panic("unknown auth method")
	}
	c.Logger = logger
	return c, nil
}

// ensureClient creates a new global client if it is not already set up
func ensureClient() {
	if client == nil {
		logger.Error("could not initialize client. Be sure you have CLOCKWORKS_USERNAME and CLOCKWORKS_TOKEN set")
		os.Exit(2)
	}
}

// initClient initializes the client. Note there is no fatal error if this
// fails, however it will be stored in the global clientErr for future
// reference. We do this so we can silently fail when doing shell completions
func initClient() {
	client, clientErr = lazyClient("viper")
}

func initLogging() {
	lopts := log.Options{
		Prefix: "🕰️ clockworksctl ",
	}
	if Verbose {
		lopts.Level = log.DebugLevel
	}
	logger = slog.New(log.NewWithOptions(os.Stderr, lopts))
	slog.SetDefault(logger)
}

func bindVMEditCmd(cmd *cobra.Command) {
	editVMCmd := NewEditVMCmd()
	cmd.AddCommand(editVMCmd)
	err := clockworks.BindEditVMCobra(editVMCmd)
	if err != nil {
		panic(err)
	}
}

func mustBindCobraGout(cmd *cobra.Command) {
	if err := goutbra.Bind(cmd, goutbra.WithField("format")); err != nil {
		panic(err)
	}
}

func bindGlobalFlags(cmd *cobra.Command) {
	cmd.SetVersionTemplate("{{ .Version }}\n")
	cmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.clockworksctl.yaml)")
	cmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "Enable verbose output")
	cmd.PersistentFlags().Bool("output-curl-string", false, "Print cURL commands when running API interactions")
}

// getSkipWait is a little helper function to return both skip-confirm and wait
// from a *cobra.Command
func getSkipWait(cmd *cobra.Command) (bool, bool) {
	skipConfirm, err := cmd.Flags().GetBool("skip-confirm")
	panicIfErr(err)
	wait, err := cmd.Flags().GetBool("wait")
	panicIfErr(err)
	return skipConfirm, wait
}

// bindSkipWait creates the appropriate flags for skip-confirm and wait
func bindSkipWait(cmd *cobra.Command) {
	cmd.PersistentFlags().Bool("skip-confirm", false, "Skip confirmation prompt")
	cmd.PersistentFlags().Bool("wait", false, "Wait for request to complete before exiting")
}
