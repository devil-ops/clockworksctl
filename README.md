# ClockworksCTL

Control clockworks from the command line!

## Installation

Download for your OS/Arch from the [release
page](https://gitlab.oit.duke.edu/devil-ops/clockworksctl/-/releases).

Homebrew user? Make sure the [devil-ops
brew](https://gitlab.oit.duke.edu/devil-ops/homebrew-devil-ops) is tapped, and
then run `brew install clockworksctl`.

## Usage

## Environment Based Login

Set your `CLOCKWORKS_USERNAME` and `CLOCKWORKS_TOKEN` environment variables

```bash
export CLOCKWORKS_USERNAME=YOUR_USERNAME
export CLOCKWORKS_TOKEN=YOUR_TOKEN
```

You can generate your `CLOCKWORKS_TOKEN` [here](https://clockworks.oit.duke.edu/api_info)

## CLI

The CLI built in help will show you the most up to date commands.

```bash
$ clockworksctl -h
...
```

List all VMs

```bash
$ clockworksctl get vm # List all VMs (defaults to yaml format)
...
$ clockworksctl get vm --format json # But in json
...
```

## Shell Completions

Shell completion uses the build in [cobra shell
completion](https://github.com/spf13/cobra/blob/main/shell_completions.md)
package. If you are using one of our prebuilt packages (homebrew or
rpm/deb/yum), the autocomplete files will be installed automatically. If you
have installed using a different method (built locally for example), you can
install the autocomplete files by using `clockworksctl complete YOURSHELL ...`.
See the `--help` for more info on that.

Completions will work without authentication for things that don't need it
(production level for example), however to auto complete items directly fromt he
API (vm auto complete for example), you must have `CLOCKWORKS_USERNAME` and
`CLOCKWORKS_TOKEN` set.

## Tips

Having trouble or want to use the web calls elsewhere? Set
`CLOCKWORKS_CURL_PRINT=true` in your environment. This will cause the app to
print out the `curl` equivalent to all the requests it's making.
